import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataframeComponent} from './dataframe.component';
import {Route, RouterModule} from '@angular/router';
import {ClarityModule} from '@clr/angular';
import {FormsModule} from '@angular/forms';

const routes: Route[] = [
  { path: '', component: DataframeComponent},
];

@NgModule({
  declarations: [
    DataframeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ClarityModule,
    FormsModule
  ]
})
export class DataframeModule { }
