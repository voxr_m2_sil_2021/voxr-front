import { Component, OnInit } from '@angular/core';
import {RService} from '../core/services/api-service/r.service';

@Component({
  selector: 'app-dataframe',
  templateUrl: './dataframe.component.html',
  styleUrls: ['./dataframe.component.scss']
})
// The main page of the application, used to manipulate dataframes
export class DataframeComponent implements OnInit {

  openModal = false;

  dataframes: string[];
  dataName: string;
  path: string;

  constructor(private rService: RService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.rService.getDataNames().subscribe(value => {
      this.dataframes = value;
    });
  }

  deleteDataFrame(dataframe: string): void {
    this.rService.deleteDataName(dataframe).subscribe(() => {
      this.load();
    });
  }

  addDataFrame(): void {
    this.rService.createData(this.path, this.dataName).subscribe(() => {
      this.toggleModal();
      this.load();
    });
  }

  toggleModal(): void {
    this.openModal = !this.openModal;
  }
}
