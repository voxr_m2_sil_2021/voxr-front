import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RService {

  constructor(protected http: HttpClient) { }

  // Get the name of all initialized dataframe
  public getDataNames(): Observable<string[]> {
    const headers = new HttpHeaders()
      .append('Authorization', `Bearer ${localStorage.getItem('token')}`);
    return this.http.get<string[]>(`${environment.apiUrl}/R/getDataNames`, {headers});
  }

  // Create a dataframe with the name chosen with a path or an url, returns ok or the error of R
  public setDataNames(): Observable<string[]> {
    const headers = new HttpHeaders()
      .append('Authorization', `Bearer ${localStorage.getItem('token')}`);
    return this.http.get<string[]>(`${environment.apiUrl}/R/getDataNames`, {headers});
  }

  // Delete the selected data, returns ok or the error of R
  public deleteDataName(dataframe: string): Observable<string[]> {
    const headers = new HttpHeaders()
      .append('Authorization', `Bearer ${localStorage.getItem('token')}`);
    const params = new HttpParams().append('dataName', dataframe);
    return this.http.delete<string[]>(`${environment.apiUrl}/R/deletData`, {headers, params});
  }

  // Creates a file with the name ask and asks the R service to save the dataframe selected from before, returns ok or the error of R
  public createData(path: string, dataName: string): Observable<string[]> {
    const headers = new HttpHeaders()
      .append('Authorization', `Bearer ${localStorage.getItem('token')}`);
    const params = new HttpParams()
      .append('path', path)
      .append('dataName', dataName)
      .append('sep', ',');
    return this.http.post<string[]>(`${environment.apiUrl}/R/setData`, null, {headers, params});
  }
}
