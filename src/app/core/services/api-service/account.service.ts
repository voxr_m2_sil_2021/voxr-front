import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginModel} from '../../class/login-model';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(protected http: HttpClient) { }

  // Log the user in, returns a valid token
  public login(model: LoginModel): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Access-Control-Allow-Headers', 'Content-Type')
      .append('Access-Control-Allow-Methods', 'POST')
      .append('Access-Control-Allow-Origin', '*');
    return this.http.post<any>(`${environment.apiUrl}/account/login`, model);
  }
}
