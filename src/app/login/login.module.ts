import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from './login.component';
import {ClarityModule} from '@clr/angular';
import {FormsModule} from '@angular/forms';
import {Route, RouterModule} from '@angular/router';

const routes: Route[] = [
  { path: '', component: LoginComponent},
];

@NgModule({
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class LoginModule { }
