import {Component, OnInit} from '@angular/core';
import {LoginModel} from '../core/class/login-model';
import {AccountService} from '../core/services/api-service/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Used by the form to store login/password
  loginModel = new LoginModel();

  showError = false;

  constructor(public accountService: AccountService, protected router: Router) {
  }

  ngOnInit(): void {
  }

  // Log the user in with provided loginModel
  login() {
    this.accountService.login(this.loginModel).subscribe({
      next: value => {
      localStorage.setItem('token', value.token);
      this.router.navigateByUrl('/dataframe');
    },
      error: err => {
        this.showError = true;
      }
  });
  }

}
