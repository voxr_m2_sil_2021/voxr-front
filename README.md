![VoxR logo](src/assets/VoxRlogo.png)

# VoxR Front

A simple web interface for VoxR API, built with [Angular](https://angular.io).

## Description

The goal of this project is to demonstrate the versatility of the VoxR API by providing a simple example interface,
built here with web technologies (Angular). In this interface, you can log in and see the dataframes present on the server. You can delete some data,
and also add some by providing a valid url.

## Visuals

![image1](.images/image1.png)
![image2](.images/image2.png)

## Installation

To install Angular on your local system, you need the following:

- [Node.js](https://nodejs.org/en/)
  (Angular requires an active LTS or maintenance LTS version of Node.js)
If you are unsure what version of Node.js runs on your system, run node -v in a terminal window.

- [npm package manager](https://www.npmjs.com/get-npm), which comes bundled with node.js
Angular, the Angular CLI, and Angular applications depend on [npm packages](https://docs.npmjs.com/about-npm) for many features and functions.

## Usage

Note that the simplest solution, that don't require any installation, is to just use the official website at this address:

🔗 [http://voxr.pages.unistra.fr/voxr-front](http://voxr.pages.unistra.fr/voxr-front)

> Nb: Make sure you use the http and not https version of the website, as the former can cause known problems with modern browsers.

If you want to run it on your local machine, after downloading the repository run `npm i` in it to install all the node modules needed by the project.

Then you can run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Contributing

The VoxR team feels that its solution is not ready for contributions yet.
We can't wait to see what you have to offer though, and will open to contributions and explains how ASAP!

## Licence

[MIT](LICENCE)
